package ua.aleksenko.authorizationservice.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TestDataConstant {

	public static final String TOKEN_PATTERN = "^[\\w-]+\\.[\\w-]+\\.[\\w-]+$";
	public static final String TEST_USER_EMAIL = "artemii@gmail.com";
	public static final String TEST_USER_FIRST_NAME = "Artemii";
	public static final String TEST_USER_LAST_NAME = "Aleksenko";
	public static final String TEST_USER_PASSWORD = "password";
}
