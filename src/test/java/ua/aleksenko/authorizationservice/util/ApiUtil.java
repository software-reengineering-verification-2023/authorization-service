package ua.aleksenko.authorizationservice.util;

import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.aleksenko.authorizationservice.util.TestDataConstant.TEST_USER_EMAIL;
import static ua.aleksenko.authorizationservice.util.TestDataConstant.TEST_USER_FIRST_NAME;
import static ua.aleksenko.authorizationservice.util.TestDataConstant.TEST_USER_LAST_NAME;
import static ua.aleksenko.authorizationservice.util.TestDataConstant.TEST_USER_PASSWORD;
import static ua.aleksenko.authorizationservice.util.TestDataConstant.TOKEN_PATTERN;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import ua.aleksenko.authorizationservice.AbstractIntegrationTest;
import ua.aleksenko.authorizationservice.model.dto.AuthenticationResponseDto;
import ua.aleksenko.authorizationservice.model.dto.LoginRequestDto;
import ua.aleksenko.authorizationservice.model.dto.RegistrationRequestDto;

@Component
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ApiUtil extends AbstractIntegrationTest {

	private final MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	public ApiUtil(MockMvc mockMvc) {
		this.mockMvc = mockMvc;
	}

	@SneakyThrows
	public AuthenticationResponseDto registerDefaultUser() {
		// Given
		RegistrationRequestDto registrationDto = new RegistrationRequestDto(
				TEST_USER_FIRST_NAME, TEST_USER_LAST_NAME, TEST_USER_EMAIL, TEST_USER_PASSWORD
		);
		// When
		String response = mockMvc.perform(post("/api/v1/registration")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(registrationDto)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.token", matchesPattern(TOKEN_PATTERN)))
				.andReturn()
				.getResponse()
				.getContentAsString();

		return objectMapper.readValue(response, new TypeReference<>() {
		});
	}

	@SneakyThrows
	public void loginWithDefaultUser() {
		// Given
		LoginRequestDto loginDto = new LoginRequestDto(
				"artemii@gmail.com", "password"
		);
		// When
		mockMvc.perform(post("/api/v1/login")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(loginDto)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.token", matchesPattern(TOKEN_PATTERN)))
				.andReturn()
				.getResponse()
				.getContentAsString();
	}
}
