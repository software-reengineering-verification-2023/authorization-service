package ua.aleksenko.authorizationservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.transaction.annotation.Transactional;

import lombok.SneakyThrows;
import ua.aleksenko.authorizationservice.AbstractIntegrationTest;
import ua.aleksenko.authorizationservice.model.dto.RegistrationRequestDto;
import ua.aleksenko.authorizationservice.model.entity.Role;
import ua.aleksenko.authorizationservice.model.entity.User;
import ua.aleksenko.authorizationservice.repository.UserRepository;
import ua.aleksenko.authorizationservice.util.ApiUtil;

@AutoConfigureMockMvc
class RegistrationControllerIT extends AbstractIntegrationTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ApiUtil apiUtil;

	@Test
	@SneakyThrows
	@Transactional
	void registerUser() {
		// Given
		RegistrationRequestDto registrationDto = new RegistrationRequestDto(
				"Artemii", "Aleksenko", "artemii@gmail.com", "password"
		);

		// When
		apiUtil.registerDefaultUser();

		// Then
		Optional<User> userOptional = userRepository.findByEmailIgnoreCase(registrationDto.getEmail());
		assertTrue(userOptional.isPresent());
		User user = userOptional.get();
		assertEquals(registrationDto.getFirstName(), user.getFirstName());
		assertEquals(registrationDto.getLastName(), user.getLastName());
		assertEquals(registrationDto.getEmail(), user.getEmail());
		assertNotNull(user.getPassword());
		assertEquals(Role.USER, user.getRole());
	}
}
