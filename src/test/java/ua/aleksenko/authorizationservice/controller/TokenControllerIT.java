package ua.aleksenko.authorizationservice.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import lombok.SneakyThrows;
import ua.aleksenko.authorizationservice.AbstractIntegrationTest;
import ua.aleksenko.authorizationservice.util.ApiUtil;

@AutoConfigureMockMvc
class TokenControllerIT extends AbstractIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ApiUtil apiUtil;

	@Test
	@SneakyThrows
	@Transactional
	void validateToken() {
		// Given
		String token = apiUtil.registerDefaultUser().token();
		// When
		// Then
		mockMvc.perform(get("/api/v1/token/validate")
						.param("token", token))
				.andExpect(status().isOk());
	}

	@Test
	@SneakyThrows
	void validateInvalidToken() {
		// Given
		String invalidToken = "Invalid Token";
		// When
		// Then
		mockMvc.perform(get("/api/v1/token/validate")
						.param("token", invalidToken))
				.andExpect(status().isUnauthorized())
				.andExpect(jsonPath("$.code", equalTo(HttpStatus.UNAUTHORIZED.value())))
				.andExpect(jsonPath("$.message", equalTo("The token was expected to have 3 parts, but got 0.")));
	}
}
