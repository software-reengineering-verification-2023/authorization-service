package ua.aleksenko.authorizationservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.transaction.annotation.Transactional;

import ua.aleksenko.authorizationservice.AbstractIntegrationTest;
import ua.aleksenko.authorizationservice.util.ApiUtil;

@AutoConfigureMockMvc
class LoginControllerIT extends AbstractIntegrationTest {

	@Autowired
	private ApiUtil apiUtil;

	@Test
	@Transactional
	void login() {
		// Given
		apiUtil.registerDefaultUser();
		// When
		// Then
		apiUtil.loginWithDefaultUser();
	}
}
